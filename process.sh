#!/bin/sh
clear
if [ $# -eq 0 ]
  then
    echo "Pass the filename as a parameter."
    exit 1
fi
FILE=$1
DURATION=$(ffprobe -i "$FILE" -show_entries format=duration -v quiet -of csv="p=0")
DURATION=$(printf %.0f $DURATION)
TOTAL_MINUTES=$((DURATION/60))
HOURS=$((TOTAL_MINUTES/60))
MINUTES=$(expr $TOTAL_MINUTES % 60)
REMAINDER=$((DURATION-(HOURS*3600)))
echo "Processing file $FILE..."
echo "   Video is $HOURS hours and $MINUTES minutes ($DURATION total seconds)."

# Initially sets the number of parts to the number of hours
PARTS=$HOURS

# If there are 20 or more remaining minutes, split it into a new part
if [ $MINUTES -gt 19 ]
then
  PARTS=$((PARTS+1))
else
  REMAINDER=$((REMAINDER+3600))
fi

# Exit if we don't have enough parts to process (less than 2 parts)
if [ $PARTS -lt 2 ]
then
  echo "   Not processing: video is less than an hour long."
  echo "Done."
  exit 0
fi

# Continuing, since we have at least an hour long video
echo "   Video will be $PARTS parts."

# Process all the parts
for i in $(seq 1 $PARTS)
do
  PART_TIME=$((i-1))
  PADDED_PART_TIME=$(printf "%02d\n" $PART_TIME)
  PADDED_PART=$(printf "%02d\n" $i)
  OUTPUT="${FILE%%.*} - Part $PADDED_PART.mp4"
  START="$PADDED_PART_TIME:00:00"

  if [ $i = $PARTS ]
  then
    LENGTH="$REMAINDER"
  else
    LENGTH="3600" # 1 hour
  fi

  printf "   Creating $OUTPUT, which starts at $START and is $LENGTH seconds long... "
  ffmpeg -loglevel 16 -ss $START -t $LENGTH -i "$FILE" -movflags faststart -vcodec copy -acodec copy "$OUTPUT"
  printf "Done.\n"
done
echo "Processing complete!"
